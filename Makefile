CC = gcc

INCDIR = include/ 
SRCDIR = src/

CCFLAGS= -g -Wall -I$(INCDIR)

SRCS:= $(wildcard $(SRCDIR)* .cpp) 
OBJS= $(subst .cc,.o,$(SRCS))
EXE= main
LIBS = -lreadline

all: $(EXEDIR)/$(EXE)

$(EXEDIR)/$(EXE): $(OBJS)
	$(CC) $(CCFLAGS) -o $(EXE)  $(OBJS) $(LIBS)

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CC) $(CCFLAGS) -MM $^>>./.depend;

clean:
	rm $(EXE)
