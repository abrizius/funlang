#include <stdlib.h>
#include <stdio.h>

#include "constants.h"
#include "memory.h" 

void ** add_dynamic_mem(void * ptr) {
    
    // only initialized on first call 
    static void * ptrs[MAX_INPUT_LENGTH] = {0};

    // find next open slot 
    int i;
    for(i = 0; i < MAX_INPUT_LENGTH; i++) {
        if (ptrs[i] == NULL) {
            ptrs[i] = ptr;
            return ptrs;
        }
    }   

    fprintf(stderr, "Error allocating ptr in tracker! Exiting...\n");
    exit(1);

}

void free_dynamic_mem(void ** mem_table) {
    int i = 0;
    while ( mem_table[i] != NULL) {
        free(mem_table[i]);
        mem_table[i] = NULL;
        i++;
    }
}
