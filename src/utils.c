#include <string.h>

#include "utils.h"

int count_chr(char * str, char chr) {
    if (!str) {
        return -1;
    }
    
    int count = 0;
    int in_single_quote = 0;
    int in_double_quote = 0;

    int i;
    for( i = 0; i < strlen(str); i++) {
        if ( str[i] == chr && !in_single_quote && !in_double_quote) {
            count++;
            if (chr == '\'' || chr == '"') continue;
        } else if ( str[i] == '"') {
            in_double_quote = ~in_double_quote;
        } else if ( str[i] == '\'') {
            in_single_quote = ~in_single_quote;
        }
    }

    return count;
}
