#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

#include "constants.h"
#include "memory.h"
#include "process.h"
#include "symbol.h"

symbol * alloc_symbol() {
    
    symbol * sym = malloc(sizeof(symbol));
    sym->set = 0;

    add_dynamic_mem(sym);

    if (!sym) {
        fprintf(stderr, "System out of memory! Exiting...\n");
        exit(1);        
    }

    return sym;
}

symbol * set_symbol(Type type, void * value) {
    symbol * sym = alloc_symbol();
    
    switch(type) {
        case NUM:
            sym->mem.number.value = *(int *) value;
            sym->type = type;
            break;
        case BOOL:
            sym->mem.boolean.value = *(char *) value;
            sym->type = type;
            break;
        case CHAR:
            sym->mem.character.value = *(char *) value;
            sym->type = type;
            break;
        case STR: ;
            size_t size = strlen((char *) value);
            sym->mem.string.value = malloc(size+1);
            strncpy(sym->mem.string.value, (char *) value, size);
            sym->mem.string.value[size] = 0;
            sym->type = type;
            break;
        case OP:
            strncpy(sym->mem.op.value, (char *) value, 2);
            sym->type = type;
            break;
        case PAREN:
            sym->mem.paren.value = *(char *) value;
            sym->type = type;
            break;
        case UNDEF:
            break;
    }    
    
    sym->set = 1;
    
    return sym;
}


Type get_token_type(char * token) {
    if (token[0] == '(' || token[0] == ')') return PAREN; 
    
    char ops[4] = "+-*/";
    int i;
    for (i = 0; i < strlen(ops); i++) {
        if (ops[i] == token[0]) {
            return OP;
        }
    }

    char * ptr;
    strtol(token, &ptr, 10);
    if (ptr != token) {
         return NUM;
    }
    
    if (strncmp(token, "#t", 2) == 0 || strncmp(token, "#f", 2) == 0) {
        return BOOL;    
    }

    int length = strlen(token);
    if ((length == 3 || length == 4) && token[0] == '\'' && token[length] == '\'') {
        return CHAR;
    }

    if (token[0] == '\"' && token[length - 1] == '\"') {
        return STR;
    }

    return UNDEF;
}


symbol * do_operation(symbol * head, symbol * left, symbol * right) {
    if (!op_defined(head->mem.op.value, left->type, right->type)){
        return NULL;
    }
    
    switch(head->mem.op.value[0]) { 
        case '+':
            return add_symbols(left, right);
            break;
        case '-':
            return sub_symbols(left, right);
            break;
        case '*':
            return mult_symbols(left, right);
            break; 
        case '/':
            return div_symbols(left, right);
            break;      
        default:
            printf("Operator %c\n not recognized\n", head->mem.op.value[0]);
    }
    return NULL;
}

symbol * add_symbols(symbol * left, symbol * right) {
    if (left->type == NUM && right->type == NUM) {
        int value = left->mem.number.value + right->mem.number.value;
        return set_symbol(NUM, &value);
    }

    if (left->type == STR && right->type == STR) {
        char value[MAX_INPUT_LENGTH];
        value[0] = '\"';
        strcpy (value + 1, left->mem.string.value);
        strcat (value, right->mem.string.value);
        int length = strlen(value);
        value[length] = '\"';
        value[length + 1] = 0;

        return set_symbol(STR, &value);        
    }
    
    fprintf(stderr, "Adding types %s and %s not yet supported\n", get_symbol_name(left->type), get_symbol_name(right->type));
    exit(1);

    return NULL; 
}

symbol * sub_symbols(symbol * left, symbol * right) {
   if (left->type == NUM && right->type == NUM) {
        int value = left->mem.number.value - right->mem.number.value;
        return set_symbol(NUM, &value);
    }else {
        fprintf(stderr, "Subtracting non-int types not yet supported\n");
    }
    return NULL; 
}

symbol * mult_symbols(symbol * left, symbol * right) {
   if (left->type == NUM && right->type == NUM) {
        int value = left->mem.number.value * right->mem.number.value;
        return set_symbol(NUM, &value);
    }else {
        fprintf(stderr, "Multiplying non-int types not yet supported\n");
    }
    return NULL; 
}

symbol * div_symbols(symbol * left, symbol * right) {
   if (left->type == NUM && right->type == NUM) {
        int value = left->mem.number.value / right->mem.number.value;
        return set_symbol(NUM, &value);
    }else {
        fprintf(stderr, "Dividing non-int types not yet supported\n");
    }
    return NULL; 
}

int op_defined(char * operation, Type left, Type right) {
    //if (left != right) {
        //fprintf(stderr, "Operator %s undefined for types '%s' and '%s'\n", operation, get_symbol_name(left), get_symbol_name(right));
        //exit(1); 
    //}
    return 1;
}

char * get_symbol_name(Type type) {
    switch(type) {
        case NUM:
            return "Number";
        case BOOL: 
            return "Boolean";
        case CHAR: 
            return "Character";
        case STR: 
            return "String";
        case OP: 
            return "Operator";
        default:
            fprintf(stderr, "Symbol type unrecognized");
    }
    return NULL;
}
