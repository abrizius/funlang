#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "io_lib.h"
#include "process.h"

int parse_arguments(int argc, char * argv[]) {
    rl_instream = NULL;
    rl_outstream = NULL;

    int opt;

    while ((opt = getopt (argc, argv, "i:o:")) != -1)
    {
        switch (opt)
        {
            case 'i':
                rl_instream = fopen(optarg, "r");            
                
                if (!rl_instream) {
                    // file does not exist
                    return -1;    
                }
                break;
             
            case 'o':
                break;
        }
    }   
    
    return 1;
}

void handle_parse_errors(int error) {
    if (error == -1) {
        puts("Invalid input file");         
    }
}

int repl() {
    char * input;
    char prompt[100];

    // auto-complete on tab press
    rl_bind_key('\t', rl_complete);

    if (rl_instream != NULL) {
        rl_outstream = fopen("/dev/null", "w"); 
    }

    while(TRUE) {
        snprintf(prompt, sizeof(prompt), "%s> ", PROMPT_NAME);
        input = readline(prompt);

        if (!input) {
            if (rl_instream == NULL || rl_instream == stdin) {
                free(input);
                return 1;
            } else {
                rl_outstream = stdout;
                rl_instream = stdin;
                free(input);
                continue;        
            } 
        }
        
        // parse the input
        process(input); 
        add_history(input);
    
        free(input);
    }
}
