#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "ast.h"
#include "memory.h"
#include "process.h"

ast_node * alloc_ast() {
    ast_node * root = malloc(sizeof(ast_node)); 
    
    add_dynamic_mem(root);

    if (!root) {
        fprintf(stderr, "System out of memory! Exiting...\n");
        exit(1);
    }
    
    root->sym = alloc_symbol();
    root->left = NULL; 
    root->right = NULL; 
    root->parent = NULL;

    return root;
}


int ast_add(ast_node ** head, char * token) {
    Type type = get_token_type(token);     
    switch(type) {
        case PAREN:
            ast_add_paren(head, token[0]);
            break;
        case OP:
            ast_add_op(*head, token[0]);
            break;
        case NUM:
            ast_add_value(*head, token, NUM);
            break;
        case BOOL:
            ast_add_value(*head, token, BOOL);
            break;
        case CHAR:
            ast_add_value(*head, token, CHAR);
            break;
        case STR:
            ast_add_value(*head, token, STR);
            break;
        default:
            return 0;
    }
    return 1;
}

int ast_add_paren(ast_node ** start, char token) {
    ast_node * head = *start;

    if (is_empty(head)) {
        if (token == ')') {
            fprintf(stderr, "Input cannot start with ')'\n");
            return 0;
        } else {
            head->sym = set_symbol(PAREN, &token); 
        }
    }
    
    switch(token) {
    case '(': ;
        char space = ' ';
        if (head->left && !(head->right)) {
            head->right = alloc_ast();
            head->right->parent = head;
            head = head->right;
            head->sym = set_symbol(OP, &space);
        } else if (!head->left) {
            head->left = alloc_ast();  
            head->left->parent = head;
            head = head->left;
            head->sym = set_symbol(OP, &space);
        } else {
            fprintf(stderr, "Error adding paren to AST!\n");
            return 0;
        }
        break;
    case ')': ;
        head = head->parent;    
        break;
    }

    *start = head;

    return 1;
}

int ast_add_op(ast_node * head, char token) {
    head->sym = set_symbol(OP, &token); 
    return 1;
}

int ast_add_value(ast_node * head, char * token, Type type) {
    int value;
    
    if (type == NUM) {
        value = atoi(token);
    } else if (type == BOOL) {
        if (token[1] == 't') value = 1;
        else value = 0;
    } else if (type == CHAR) {
        if (strlen(token) == 2) value = '\n';
        else value = token[0];
    } else if (type == STR) {
        token[strlen(token) - 1] = 0;   
    }

    if (head->left && !(head->right)) {
        head->right = alloc_ast();
        head->right->parent = head;
        if (type != STR)  head->right->sym = set_symbol(type, &value);
        else head->right->sym = set_symbol(type, token + 1);
    } else if (!head->left) {
        head->left = alloc_ast();
        head->left->parent = head;
        if (type != STR)  head->left->sym = set_symbol(type, &value);
        else head->left->sym = set_symbol(type, token + 1);
    } else {
        fprintf(stderr, "Error adding %d to AST!\n", value);
        return 0;
    }
    
    return 1;
}

int ast_add_str(ast_node * head, char * token) {
    return 1;
}

void ast_walk(ast_node ** start) {
    
    ast_node * head = *start;
    if (head == NULL) {
        return;
    }
    
    if (head->left != NULL && head->right != NULL) { 
        ast_walk(&(head->left));
        ast_walk(&(head->right)); 

    } else {

        if (head->parent->left->sym->type == OP ||
            head->parent->right->sym->type == OP) {
            return;
        }
        

        head->parent->sym = do_operation(head->parent->sym,
                                         head->parent->left->sym,
                                         head->parent->right->sym);  
        head->parent->left = NULL;
        head->parent->right = NULL;
        
    }
}


int is_empty(ast_node * head) {
    return (!head->sym->set);
}
