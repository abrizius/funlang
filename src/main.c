#include "io_lib.h"

int main(int argc, char * argv[]) {
    int ret;

    // parse command line arguments 
    ret = parse_arguments(argc, argv);       
    if (!ret) {
        handle_parse_errors(ret);
        return 1;
    }
    
    // Read - Eval - Print Loop
    repl();

    // clean-up
    return 0;
}
