#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"
#include "constants.h"
#include "memory.h"
#include "parse.h"
#include "process.h"
#include "utils.h"

int parse(char * input) {
    
    // check the number of parens, single quotes, and doubles quotes
    if( count_chr(input, '(') + count_chr(input, ')') % 2 == 1 ||
        count_chr(input, '"') % 2 == 1 || count_chr(input, '\'') % 2 == 1) {
        fprintf(stderr, "Invalid input (Unmatched parens or quotes)!\n");
        return 0;
    }
        
    // pad each paren with spaces 
    if (!pad_parens(&input)) {
        fprintf(stderr, "Error padding input %s\n", input);       
        return 0;
    }

    // 'input' is now dynamically allocated, add to tracking table
    void ** mem_table = add_dynamic_mem(input);
    
    ast_node * tree = alloc_ast();
    ast_node * root = tree;

    char * token = strtok(input, " ");
    while (token) {        
        if (!ast_add(&tree, token)) {
            return 0;
        }
        
        token = strtok(NULL, " ");
    }
    
    while(root->sym->type == OP) {
        ast_walk(&root);
    }

    print(root->sym);

    //now free mem
    free_dynamic_mem(mem_table);    
      
    return 1;
}

   

int pad_parens(char ** input_ptr) {
    char * input = *input_ptr;

    // count the number of parens 
    int count = 0;
    int length = strlen(input);
    int i;
    for (i = 0; i < length; i++) {
        if (is_paren(input[i])) count++;
    }
    
    char * padded_string = calloc((length + 3 * count + 1), sizeof(char));
    if (!padded_string) {
        return 0;
    } 

    int j = 0;
    for (i = 0; i < length; i++) {
       if (is_paren(input[i])) {
            padded_string[j] = ' ';
            padded_string[j+1] = input[i];
            padded_string[j+2] = ' ';
            j += 3;
       } else {
            padded_string[j] = input[i];
            j++;
       } 
    }
    
    *input_ptr = padded_string;
    
    return 1;
}   

int is_paren(char input) {
    return (input == ')' || input == '('); 
}


