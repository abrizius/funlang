#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/history.h>

#include "parse.h"
#include "process.h"

int handle_command(char * command) {
    // determine the command 
    char * word = strtok(command, " "); 
    if (!word) {
        return 0;
    }
    
    if (strcmp(command, "write") == 0) {
        handle_write(strtok(NULL, " "));
    }       
    
    return 1;
}

int handle_write(char * file) {
    if (!file) {
        return 0;
    }
    
    write_history(file);    
    return 1;
}

int process(char * input) {
    int size = strlen(input);
    if (size <= 0) {
        return 0;
    }

    if (input[0] == ';') {
        // get the substring
        char * substr = malloc(size - 2);
        memcpy(substr, input + 1, size);
        handle_command(substr);
        free(substr);        
        return 1;
    }
    
    parse(input);

    return 1;
}


void print(symbol * sym) {
    if (sym->type == NUM) {
        printf(">> %d\n", sym->mem.number.value);
    } else if (sym->type == BOOL) {
        if (sym->mem.boolean.value) puts(">> #t"); 
        else puts(">> #f");
    } else if (sym->type == CHAR) {
        printf(">> \'%c\'\n", sym->mem.character.value);
    } else if (sym->type == STR) {
        printf(">> %s\n", sym->mem.string.value);
    } else if (sym->type == OP) {
        printf(">> %s\n", sym->mem.op.value);
    }
}

