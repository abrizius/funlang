#ifndef IO_LIB_H
#define IO_LIB_H

#include "constants.h"

#define HISTORY_LENGTH 100

int parse_arguments(int, char **);
void handle_parse_errors(int);
int repl();

#endif
