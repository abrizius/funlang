#ifndef PARSE_H
#define PARSE_H

int parse(char *);
void unpad(char *);
char * get_next_token(char *, int *);
int pad_parens(char **);
int is_paren(char);

#endif
