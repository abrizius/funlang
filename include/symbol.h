#ifndef SYMBOL_H
#define SYMBOL_H

typedef enum {NUM, BOOL, CHAR, STR, OP, PAREN, UNDEF} Type;

typedef struct symbol {
    Type type;
    int set;

    union {
        struct {
            int value;
        } number;
        struct {
            char value;
        } boolean;
        struct {
            char value;
        } character;
        struct {
            char * value;
        } string;
        struct {
            char value[2];
        } op;
        struct {
            char value;
        } paren;
    } mem;  
} symbol;

symbol * alloc_symbol();
symbol * set_symbol(Type, void *);
Type get_token_type(char *);
symbol * do_operation(symbol *, symbol *, symbol *);
int op_defined(char *, Type, Type);
char * get_symbol_name(Type);

symbol * add_symbols(symbol *, symbol *);
symbol * sub_symbols(symbol *, symbol *);
symbol * mult_symbols(symbol *, symbol *);
symbol * div_symbols(symbol *, symbol *);

#endif
