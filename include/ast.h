#ifndef AST_H
#define AST_H

#include "symbol.h"

typedef struct ast_node{
    symbol * sym;
    struct ast_node * left;
    struct ast_node * right;
    struct ast_node * parent;
} ast_node;

ast_node * alloc_ast();
ast_node * alloc_node();
int ast_add(ast_node **, char *);
int ast_add_paren(ast_node **, char);
int ast_add_op(ast_node *, char);
int ast_add_value(ast_node *, char *, Type);

int ast_add_bool(ast_node *, char );
int ast_add_char(ast_node *, char *);
int ast_add_str(ast_node *, char *);

void ast_walk(ast_node **);

int is_empty(ast_node *);

#endif
