#ifndef MEMORY_H
#define MEMORY_H

void ** add_dynamic_mem(void *);
void free_dynamic_mem(void **);

#endif
