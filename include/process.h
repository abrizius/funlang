#ifndef PROCESS_H
#define PROCESS_H

#include "symbol.h" 

int process(char *);
int handle_command(char *);
int handle_write(char *);
void print(symbol *);

#endif
